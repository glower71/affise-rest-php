##affise-php-rest
A simple PHP library for using Affise REST API

[Affise API Documentation](https://api.affise.com/docs3.1/)

##Requirements
- php: >=7.0.0
- ext-json: *
- ext-curl: *
        
## Example ##
``` php
use Shilov\AffiseRest\AffiseRest;

$rest = new AffiseRest($domain, $apiKey, '3.0');

$resultStatsCustom = $rest->get('stats/custom', [
   'filter' => [
        'advertiser' => 1
    ]
]);

$resultPartnerActivationOffer = $rest->post('partner/activation/offer', [
    'offer_id' => 1,
    'comment' => 'some comment',
]);

$result = $rest->delete('admin/offer/source/' . $id);
```        
##Installation
    composer require shilov/affise-rest-php:dev-master

##Author
Andrei Shilov - glower71@gmail.com - https://www.linkedin.com/in/shilov-a/        
