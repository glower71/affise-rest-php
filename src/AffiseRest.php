<?php namespace Shilov\AffiseRest;

use Shilov\AffiseRest\Exceptions\CurlException;
use Shilov\AffiseRest\Exceptions\RequestException;

class AffiseRest
{
    protected $domain;
    protected $apiKey;
    protected $version;

    public function __construct($domain, $apiKey, $version = '3.0')
    {
        $this->domain = $domain;
        $this->apiKey = $apiKey;
        $this->version = $version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @param $path
     * @param $params
     * @return array
     * @throws CurlException
     * @throws RequestException
     */
    public function get($path, $params)
    {
        return $this->call($path, 'get', $params);
    }

    /**
     * @param $path
     * @param $params
     * @return array
     * @throws CurlException
     * @throws RequestException
     */
    public function post($path, $params)
    {
        return $this->call($path, 'post', $params);
    }

    /**
     * @param $path
     * @return array
     * @throws CurlException
     * @throws RequestException
     */
    public function delete($path)
    {
        return $this->call($path, 'delete');
    }

    /**
     * @param string $path
     * @param string $method
     * @param array $params
     * @return array
     * @throws CurlException
     * @throws RequestException
     */
    protected function call(string $path, string $method, array $params = [])
    {
        $url = 'https://api-' . $this->domain . '.affise.com/' . $this->version . '/' . $path;
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_HTTPHEADER => [
                'Api-Key: ' . $this->apiKey,
            ],
            CURLOPT_RETURNTRANSFER => true,
        ]);

        if ($method == 'get') {
            $url .= '?' . http_build_query($params);
        }

        if ($method == 'post') {
            curl_setopt_array($ch, [
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => http_build_query($params),
            ]);
        }

        if ($method == 'delete') {
            curl_setopt_array($ch, [
                CURLOPT_URL => $url,
                CURLOPT_CUSTOMREQUEST => 'DELETE',
            ]);
        }

        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
        ]);

        return $this->curlExec($ch);
    }

    /**
     * @param $ch
     * @return array
     * @throws CurlException
     * @throws RequestException
     */
    public function curlExec($ch)
    {
        $curlResult = curl_exec($ch);
        if ($curlResult === false) {
            throw new CurlException('Curl request error');
        }

        $result = json_decode($curlResult, true);
        if ($result['status'] == 1) {
            return $result;
        } elseif ($result['status'] == 2) {
            throw new RequestException($result['error']);
        }

        return $curlResult;
    }
}
